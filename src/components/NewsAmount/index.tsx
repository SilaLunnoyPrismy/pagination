import * as React from 'react';
interface INewsAmount {
    newsAmount: number;
}
interface INewsProps {
    update: any;
}
export class NewsAmount extends React.Component<INewsProps, INewsAmount> {
    constructor(props: INewsProps) {
        super(props);
        this.change = this.change.bind(this);
        this.state = {
            newsAmount: 5
        }
    }
    public change(event: React.ChangeEvent<HTMLSelectElement>) {
        this.props.update(event.target.value);
        this.setState({
            newsAmount: parseInt(event.target.value, 10)
        });
    }
    public render() {
        return (
            <div className="col-md-3">
                <select className="mdb-select md-form" onChange={this.change} value={this.state.newsAmount}>
                    <option value="6">6 новостей</option>
                    <option value="9">9 новостей</option>
                    <option value="12">12 новостей</option>
                    <option value="15">15 новостей</option>
                </select>
                <label>Количество новостей на странице</label>
            </div>
        );
    }
}
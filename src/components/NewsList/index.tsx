import * as React from 'react';
import { INewsItem, NewsItem } from '../NewsItem';
export interface INewsListProps<T extends INewsItem> {
    dataSource: T[];
}
export interface INewsListState {
    dataUrl: string;
}
export class NewsList<T extends INewsItem> extends React.Component<INewsListProps<T>, INewsListState>{
    public render() {
        return this.props.dataSource.map((el, key) => {
            return (<NewsItem data={el} key={key}/>)
        });
    }
}
import * as React from 'react'
export interface IPagination {
    curPage: number;
    maxPage: number;
    onPageSelect: (page: number) => any;
    onIncPage: () => any;
    onDecPage: () => any;
}

export const Pagination: React.SFC<IPagination> = (props: IPagination) => {
    /*const { onPageSelect } = props;
    const onPageSelectEv = (page:number) => {
        if(onPageSelect){
            onPageSelect(page);
        }
    }*/
    const pages = Array(props.maxPage).fill(1)
        .map((x, ind) =>
            <li onClick={props.onPageSelect.bind(Pagination, ind)}
                className={"page-item" + (ind === props.curPage ? " active" : "")} key={ind}>
                <a className="page-link">
                    {ind}
                </a>
            </li>
        );

    return (
        <div className="col-md-5 text-center mb-5 mt-3">
            <nav aria-label="pagination example">
                <ul className="pagination pagination-circle pg-blue mb-0">

                    <li onClick={props.onDecPage}
                        className={"page-item" + (props.curPage === 0 ? " disabled" : "")}>
                        <a className="page-link" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span className="sr-only">Previous</span>
                        </a>
                    </li>
                    {pages}
                    <li onClick={props.onIncPage}
                        className={"page-item" + (props.curPage === props.maxPage - 1 ? " disabled" : "")}>
                        <a className="page-link" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span className="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav >
        </div>
    )
}

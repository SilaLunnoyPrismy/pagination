import * as React from 'react';
import './App.css';
import { NewsListWrapper } from './components/NewsListWrapper';
import { giveAwesomePowers, person } from './components/Person';
class App extends React.Component {
  public state = { count: 0 };
  public inc(): any {
    this.setState({ count: this.state.count + 1 });
  }
  public render() {
    console.log(person);
    const b = giveAwesomePowers(person);
    console.log(b);
    console.log(b===person);
    return (
      <div className="container">
        <NewsListWrapper dataUrl="https://node.black-d.ga/news" />
      </div>
    );
  }
}

export default App;

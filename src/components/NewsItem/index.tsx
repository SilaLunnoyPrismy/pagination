import * as React from 'react';
export interface INewsItem {
    id: number;
    text: string;
    author: string;
    type: string;
}
export interface INewsItemState {
    expand: boolean;
}
export interface INewsItemProps<T extends INewsItem> {
    data: T;
}
export class NewsItem<T extends INewsItem> extends React.Component<INewsItemProps<T>, INewsItemState>{
    public render() {
        return (
            <div className="col-md-4 p-3">
                <div className="card">

                    <div className="card-body">
                        <h4 className="card-title">
                            <a className="text-left">{this.props.data.author}</a>
                        </h4>
                        <p className="card-text">{this.props.data.text}</p>
                        <span className="red-text p-1">{this.props.data.id}</span>
                    </div>

                </div>
            </div>
        );
    }
}
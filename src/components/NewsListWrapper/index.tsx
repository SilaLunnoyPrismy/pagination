import * as React from 'react';
import { NewsAmount } from '../NewsAmount';
import { INewsItem } from '../NewsItem';
import { NewsList } from '../NewsList';
import { Pagination } from '../Pagination';

export interface INewsListWrapperProps {
    dataUrl: string;
}
export interface INewsListWrapperState<T extends INewsItem> {
    data: T[];
    curPage: number;
    limit: number;
    maxPage: number;
}
export class NewsListWrapper<T extends INewsItem> extends React.Component<INewsListWrapperProps, INewsListWrapperState<T>>{
    public constructor(props: INewsListWrapperProps) {
        super(props);
        this.state = {
            curPage: 0,
            data: [],
            limit: 6,
            maxPage: 10
        }
    }
    public updateLimitState = (value: number) => {
        this.setState({
            limit: value
        });
    }
    public componentDidMount() {
        const { dataUrl } = this.props;
        fetch(dataUrl).then(response => response.json())
            .then(response => this.setState({
                curPage: response.curPage,
                data: response.data,
                maxPage: response.maxPage
            }));
    }
    public getData(){
        let { dataUrl } = this.props;
        dataUrl = dataUrl + "?limit=" + this.state.limit + "&page=" + this.state.curPage;
        fetch(dataUrl).then(response => response.json())
            .then(response => this.setState({
                data: response.data,
                maxPage: parseInt(response.maxPage,10)
            }));
    }
    public pageSelect = (page: number) => {
        this.setState({
            curPage: page
        }, () => {
            this.getData();
        });
    }
    public incPage = () => {
        this.setState({
            curPage: this.state.curPage + 1
        });
    }
    public decPage = () => {
        this.setState({
            curPage: this.state.curPage - 1
        });
    }
    public render() {
        return (
            <div className="row">
                <NewsList dataSource={this.state.data} />
                <div className="col-md-12">
                    <div className="row">
                        <Pagination
                            onIncPage={this.incPage}
                            onDecPage={this.decPage}
                            onPageSelect={this.pageSelect}
                            curPage={this.state.curPage}
                            maxPage={this.state.maxPage} />
                        <NewsAmount update={this.updateLimitState} />
                    </div>
                </div>
            </div>
        );
    }
}